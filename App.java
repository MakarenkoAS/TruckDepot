package ua.org.oa.makarenkoas;

/*
 * @version 1.0 26 Feb 2017 
 * @author Makarenko Alexander
 */

public class App {
	
	public static void main(String[] args) {
		
		TruckDepot depot = new TruckDepot(5);
		depot.addTrucks(new Trucks("KamAZ", 6), 0);
		depot.addTrucks(new Trucks("MAZ", 8), 1);
		depot.addTrucks(new Trucks("KrAZ", 6), 2);
		depot.addTrucks(new Trucks("Volvo", 12), 3);
		depot.addTrucks(new Trucks("MAN", 10), 4);
		
		System.out.println(depot);
		
		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		
		System.out.println("The average truck tonnage in depot is "
		+ depot.calculateAvrgTonnage() + " tonns;");
		
		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		
		depot.compareTrucksToAvgTonnage(depot.calculateAvrgTonnage());
		
		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		depot.setTargetValue(9.5);
		depot.compareTrucksToTargetValue();
		}
}
