package ua.org.oa.makarenkoas;

import java.util.Arrays;

/*
 * @version 1.0 26 Feb 2017 
 * @author Makarenko Alexander
 */

public class TruckDepot {

	private Trucks[] myTrucks;				  // Array, type Trucks
	private double targetValue;
	final private static int MIN_TONNAGE = 0; // Constant fields
	final private static int MAX_TONNAGE = 30; // for setTargetValue method

	// Getter&setter methods for field targetValue
	public double getTargetValue() {
		return targetValue;
	}

	public void setTargetValue(double targetValue) {
		if (targetValue > MIN_TONNAGE && targetValue < MAX_TONNAGE) {
			this.targetValue = targetValue;
		} else {
			System.out.println("Enter valid value from " + MIN_TONNAGE + " to " + MAX_TONNAGE + " tonns");
		}

	}// End of getter&setter block

	public TruckDepot(int size) {
		
		// TruckDepot constructor
		myTrucks = new Trucks[size];
	}

	public void addTrucks(Trucks trucks, int index) {
		
		// Method to add Object, type Trucks into Array type TruckDepot
		myTrucks[index] = trucks;
	}

	public double calculateAvrgTonnage() {

		// Calculate average truck tonnage in depot
		double totalTrucksTonnage = 0;
		double avgTonnage = 0;
		for (int i = 0; i < myTrucks.length; i++) {

			totalTrucksTonnage += myTrucks[i].getTonnage();
			avgTonnage = totalTrucksTonnage / myTrucks.length;

		}
		return avgTonnage;
	}

	public void compareTrucksToAvgTonnage(double avgTonnage) {

		// Compare trucks tonnage to average tonnage
		for (int i = 0; i < myTrucks.length; i++) {

			if (myTrucks[i].getTonnage() > avgTonnage) {
				System.out.println("Truck " + myTrucks[i].getName() + " tonnage is " + myTrucks[i].getTonnage()
						+ " , it's more then average;");
			} else {
				System.out.println("Truck " + myTrucks[i].getName() + " tonnage is " + myTrucks[i].getTonnage()
						+ " , it's less then average;");
			}
		}

	}

	public void compareTrucksToTargetValue() {

		// Compare trucks tonnage to target value tonnage
		for (int i = 0; i < myTrucks.length; i++) {

			if (myTrucks[i].getTonnage() > targetValue) {
				System.out.println("Truck " + myTrucks[i].getName() + " tonnage is " + myTrucks[i].getTonnage()
						+ " , it's more then target value: " + targetValue);
			} else {
				System.out.println("Truck " + myTrucks[i].getName() + " tonnage is " + myTrucks[i].getTonnage()
						+ " , it's less then target value: " + targetValue);
			}
		}

	}

	public String toString() {

		// Overloaded method toString

		return "Trucks info: " + Arrays.toString(myTrucks);
	}

}
