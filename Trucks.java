package ua.org.oa.makarenkoas;

/*
 * @version 1.0 26 Feb 2017 
 * @author Makarenko Alexander
 */

public class Trucks {
	final private static int MIN_TONNAGE = 0;	// Constant fields 
	final private static int MAX_TONNAGE = 30;	// for setTonnage method
	
	private String name;						// Name of Truck
	private double tonnage;						// Tonnage of truck
	
	public Trucks(){
		// Trucks constructor
	}

	public Trucks(String name, double tonnage) {
		
		// Overloaded Trucks constructor
		
		setName(name);
		setTonnage(tonnage);
	}

	// Getter&setter methods for field name
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}// End of getter&setter block

	
	// Getter&setter methods for field tonnage
	public double getTonnage() {
		return tonnage;
	}

	public void setTonnage(double tonnage) {
		if (tonnage > MIN_TONNAGE && tonnage < MAX_TONNAGE) {
			this.tonnage = tonnage;
		} else {
			System.out.println("Enter valid value from "  + MIN_TONNAGE + " to " + MAX_TONNAGE + " tonns");
		}
	}// End of getter&setter block

	public String toString() {

		// Overloaded method toString

		return "\n" + "(" + "name: " + name + " / tonnage: " + tonnage + " Tonns)";
	}

}
